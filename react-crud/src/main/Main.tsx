import React, {useEffect, useState} from 'react';
import {Product} from "../interfaces/product";
import { Link } from 'react-router-dom';
import Wrapper from "../admin/Wrapper";


const Main = () => {
    const [products, setProducts] = useState([] as Product[]);

    useEffect(() => {
        (
            async () => {
                const response = await fetch('http://a65b4482f473240b3916cf711d305149-1245760235.us-east-1.elb.amazonaws.com:8000/api/products');

                const data = await response.json();

                setProducts(data);
            }
        )();
    }, []);

    const like = async (id: number) => {
        await fetch(`http://a3bfb1e247a8f4503b0757c1ff76a91c-438671317.us-east-1.elb.amazonaws.com:5000/api/products/${id}/like`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'}
        });

        setProducts(products.map(
            (p: Product) => {
                if (p.id === id) {
                    p.likes++;
                }

                return p;
            }
        ));
    }

    return (
        <Wrapper>
            <main role="main">
                <div className="album py-5 bg-light">
                    <div className="container">
                        <div className="row">
                            {products.map(
                                (p: Product) => {
                                    return (
                                        <div className="col-md-4" key={p.id}>
                                            <div className="card mb-4 shadow-sm">
                                                <img src={p.image} height="180" alt="product_image"/>
                                                <div className="card-body">
                                                    <p className="card-text">{p.title}</p>
                                                    <div className="d-flex justify-content-between align-items-center">
                                                        <div className="btn-group">
                                                            <button type="button"
                                                                    className="btn btn-sm btn-outline-secondary"
                                                                    onClick={() => like(p.id)}
                                                            >
                                                                Like
                                                            </button>
                                                        </div>
                                                        <small className="">{p.likes} likes</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }
                            )}
                        </div>
                        <Link className="btn-outline-success p-3" to="/admin/products" > Add product</Link>
                    </div>
                </div>

            </main>
        </Wrapper>
    );
};

export default Main;
