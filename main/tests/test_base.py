from flask_testing import TestCase
from flask import current_app, url_for, jsonify
import requests

from main import app, Product


class MainTest(TestCase):
    def create_app(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False  # We're not going to use the CSRF (Cross Site Request For Generate Token) of forms

        return app
    
    def test_app_exists(self):
        self.assertIsNotNone(current_app)

    def test_app_in_test_mode(self):
        self.assertTrue(current_app.config['TESTING'])
        
    # def test_connection_between_services(self):
    #     response = requests.get('http://172.17.0.1:8000/api/user')

    #     self.assert200(response)

    def test_products_query(self):
        response = jsonify(Product.query.all())
        self.assert200(response)

    # # Testing if after the route /hello can't accept POST methods
    # def test_hello_post(self):
    #     response = self.client.post(url_for('hello'))

    #     self.assertEqual(response.status_code, 405)

    # def test_auth_blueprint_exists(self):
    #     self.assertIn('auth', self.app.blueprints)

    # def test_auth_login_get(self):
    #     response = self.client.get(url_for('auth.login'))

    #     self.assert200(response)

    # # Check if the route auth/login uses the template login.html
    # def test_auth_login_template(self):
    #     self.client.get(url_for('auth.login'))

    #     self.assertTemplateUsed('login.html')

    # def test_auth_login_post(self):
    #     fake_form = {
    #         'username': 'fake',
    #         'password': 'fake-password'
    #     }

    #     response = self.client.post(url_for('auth.login'), data=fake_form)
    #     self.assertRedirects(response, url_for('index'))
