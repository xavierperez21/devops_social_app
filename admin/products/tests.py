from django.http import response
from django.test import TestCase
from django.test import Client

# Create your tests here.
class ProductsTestCase(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_list_products(self):
        response = self.client.get('/api/products')

        self.assertEqual(response.status_code, 200)
        # Check that the rendered context contains 5 customers.
        # self.assertEqual(len(response.context['customers']), 5)
    
    def test_create_product(self):
        response = self.client.post('/api/products', {
            "title": "Product 4",
            "image": "https://i.imgur.com/XinKWs1.png",
            "likes": 0
        })

        self.assertEqual(response.status_code, 201)