pipeline {
  environment {
    djangoImage = ''
    flaskImage = ''
    reactImage = ''
    DOCKER_BUILDKIT = '1'
  }
  agent any
  stages {
    stage('Clean existing docker containers') {
      steps {
        script{
        echo '=================== INITIAL CLEANING STAGE ==================='
        sh 'docker-compose down'
        sh 'docker image prune -af'
        sh 'rm *.yaml || true'
        if ("${env.BRANCH_NAME}" == "master") {
          catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
            withAWS(credentials: 'aws_credentials', region: 'us-east-1') {
              sh 'kubectl delete deployment django-backend '
              sh 'kubectl delete deployment flask-backend '
              sh 'kubectl delete deployment flask-queue '
              sh 'kubectl delete deployment django-queue '
              sh 'kubectl delete deployment react-crud '
            }
          }
        }
        sh 'exit 0'
      }
     }
    }
    stage('Presteps') {
      steps {
        echo "=================== SETTING UP DOCKER-COMPOSE ==================="
        sh 'DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose up --build -d django_backend flask_backend'
      }
    }
    stage('App testing') {
      steps {
        echo '=================== RUNNING TEST STAGE ==================='
        echo "---- Fetch API response"
        sh 'sleep 3s'
        sh 'curl -I 0.0.0.0:8000/api/products'
        echo "---- Testing django_backend"
        sh "docker-compose exec -T django_backend bash -c 'python /app/manage.py test'"
        echo "---- Testing flask_backend"
        sh "docker-compose exec -T flask_backend bash -c 'flask test'"

      }
    }
    stage('Build dockerfile images') {
      steps {
        script {
          echo '=================== RUNNING IMAGE BUILD STAGE ==================='
          if ("${env.BRANCH_NAME}" == "master") {
            djangoImage = docker.build("django_service_devops_project:latest", "-f admin/Dockerfile ./admin")
            flaskImage = docker.build("flask_service_devops_project:latest", "-f main/Dockerfile ./main")
            reactImage = docker.build("react_devops_project:latest", "-f react-crud/Dockerfile ./react-crud")
          } else {
            djangoImage = docker.build("django_service_devops_project:${env.BRANCH_NAME}_$BUILD_NUMBER", "-f admin/Dockerfile ./admin")
            flaskImage = docker.build("flask_service_devops_project:${env.BRANCH_NAME}_$BUILD_NUMBER", "-f main/Dockerfile ./main")
            reactImage = docker.build("react_devops_project:${env.BRANCH_NAME}_$BUILD_NUMBER", "-f react-crud/Dockerfile ./react-crud")
          }

        }
      }
    }

    stage('Push images to ECR') {
      steps {
        script {
          echo '=================== PUSH WORKING IMAGES TO ECR ==================='
          docker.withRegistry(
            'https://899449812039.dkr.ecr.us-east-1.amazonaws.com',
            'ecr:us-east-1:aws_credentials') {

            djangoImage.push()
            flaskImage.push()
            reactImage.push()
          }
        }
      }
    }
    stage('Transpile working docker-compose to Kubernetes') {
      when {
        branch 'master'
      }
      steps {
        echo '=================== DOCKER-COMPOSE TRANSPILE STAGE ==================='
        sh 'kompose --file ./create_kompose/docker-compose.yml convert'
      }
    }
    stage('Deploy ECR Images to EKS cluster') {
      when {
        branch 'master'
      }
      steps {
        script {
          echo '=================== RUNNING IMAGE DEPLOYMENT TO EKS cluster ==================='
          docker.withRegistry(
            'https://899449812039.dkr.ecr.us-east-1.amazonaws.com',
            'ecr:us-east-1:aws_credentials') {
            withAWS(credentials: 'aws_credentials', region: 'us-east-1') {
              sh 'aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 899449812039.dkr.ecr.us-east-1.amazonaws.com'
              sh 'aws eks --region us-east-1 update-kubeconfig --name eks-test'
              sh 'kubectl get nodes'
              sh 'kubectl apply -f django-backend-deployment.yaml'
              sh 'kubectl apply -f flask-backend-deployment.yaml'
              sh 'kubectl apply -f django-queue-deployment.yaml'
              sh 'kubectl apply -f flask-queue-deployment.yaml'
              sh 'kubectl apply -f react-crud-deployment.yaml'
              echo '==  Deployments =='
              sh 'kubectl get deployments'
              echo '==  Pods =='
              sh 'kubectl get pods'
              echo '==  Services =='
              sh 'kubectl get svc'
              echo '====== The react service may take a few minutes to be reachable  ====='
            }
          }
        }
      }
    }
    stage('Cleaning up') {
      steps {
        echo '=================== SETTING DOWN DOCKER-COMPOSE ==================='
        sh 'docker-compose down'
      }

    }

  }

}