## Project Introduction

"Devops social app" is a microservices application where you can upload images to the social network and receive likes in your images.

This application uses the Amazon Elastic Kubernetes Service (EKS) to host our kubenrnetes cluster where we will have 5 microservices:

- **Admin backend service:** Where you can do CRUD operations, using **Django** as framework.
- **Admin backend queue**: This is the queue of the Admin service which will receive the events from the Main service through RabbitMQ to update its database.
- **Main backend service:** Where you can list products and give likes, using **Flask** as a framework.
- **Main backend queue**: This is the queue of the Main service which will receive the events from the Admin service through RabbitMQ to update its database.
- **Frontend service:** Using React.js as framework. This service will make requests to both backend services to show the products in a UI for the client.

You can see the infrastructure repo here: https://gitlab.com/carobitap/sysdig-devops-demo

If you have noticed, we will use 2 backend services. Each backend service will use an **Amazon RDS** service with a MySQL instance.

We will use **RabbitMQ** to connect both backend containers through a channel of events so we can update the data of both databases. This is mainly to detect the event of a "Like" in the main service (Flask) so the Admin service (Django) can detect it so it can update its database too.

Here's a diagram of how it will look the architecture of this microservices application.

![app_diagram](https://i.imgur.com/h7z52Nd.png)

## Overview

In the main app we will be able to give a "Like" to the products, and in the Admin app we can do CRUD operations with the products.

***Main app:***
![main_app](https://i.imgur.com/OmOHMXx.png)

&nbsp;

***Admin app:***
![admin_app](https://i.imgur.com/t5XEw7u.png)

&nbsp;

## Instructions to use the application
**Requirements:**
- Install Git: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- Install Docker: https://docs.docker.com/engine/install/
- Install Docker compose: https://docs.docker.com/compose/install/

&nbsp;

**Steps to use the application in your local machine**:

1. Clone the repo: [https://gitlab.com/xavierperez21/devops_social_app.git](https://gitlab.com/xavierperez21/devops_social_app)

    ```bash
    git clone https://gitlab.com/xavierperez21/devops_social_app.git
    ```

    ![https://i.imgur.com/ZClhQjE.png](https://i.imgur.com/ZClhQjE.png)

    &nbsp;

2. Change to the directory of the repo and make a **`docker-compose up`**.

    ![https://i.imgur.com/5eYkgCd.png](https://i.imgur.com/5eYkgCd.png)

    ![https://i.imgur.com/i954z8a.png](https://i.imgur.com/i954z8a.png)

    &nbsp;

3. Once all the containers are up, you can go to the route **http://localhost:3000**

    ![https://i.imgur.com/nSkkdO2.png](https://i.imgur.com/nSkkdO2.png)

    &nbsp;

4. Once you are there you may find different products. Go to **http://localhost:3000/admin/products** and **add** a new product.

    ![https://i.imgur.com/3z2zERv.png](https://i.imgur.com/3z2zERv.png)

    &nbsp;

5. You can add as much products as you want. You can also edit, or delete them.
6. Go again to **http://localhost:3000** and see all the products you've just created.
7. You can give likes to every product.

    ![https://i.imgur.com/x1P48PH.png](https://i.imgur.com/x1P48PH.png)

    &nbsp;

8. While you are doing all those steps, check the logs in your terminal to see how the HTTP requests are being done. Also, when you give a "like", you will see the logs of the queues of RabbitMQ to confirm that the other service has received the event (these queues are the containers named **flask_queue** or **django_queue**).

    ![https://i.imgur.com/CafNecx.png](https://i.imgur.com/CafNecx.png)

    &nbsp;

9. If you find issues let me know, and thank you for your feedback! :)
